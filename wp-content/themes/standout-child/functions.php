<?php

add_action('after_setup_theme', 'uncode_language_setup');

function uncode_language_setup()

{

	load_child_theme_textdomain('uncode', get_stylesheet_directory() . '/languages');

}



function theme_enqueue_styles()

{

	$production_mode = ot_get_option('_uncode_production');

	$resources_version = ($production_mode === 'on') ? null : rand();

	$parent_style = 'uncode-style';

	$child_style = array('uncode-custom-style');

	wp_enqueue_style($parent_style, get_template_directory_uri() . '/library/css/style.css', array(), $resources_version);

	wp_enqueue_style('child-style', get_stylesheet_directory_uri() . '/style.css', $child_style, $resources_version);



	wp_register_script( 'custom', get_stylesheet_directory_uri() . '/js/custom.js', array( 'jquery' ), '1.0', true );

	wp_enqueue_script( 'custom' );

}

add_action('wp_enqueue_scripts', 'theme_enqueue_styles');

function remove_gravity_forms_nag() {
	update_option( 'rg_gforms_message', '' );
	remove_action( 'after_plugin_row_gravityforms/gravityforms.php', array( 'GFForms', 'plugin_row' ) );
}
add_action( 'admin_init', 'remove_gravity_forms_nag' );